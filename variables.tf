variable "public" {
  type        = string
  description = "Whether the DB Instance should be publicly accessible"
}

variable "name" {
  type        = string
  description = "The name of the database excluding the environment"
}

variable "environment" {
  type        = string
  description = "The current environment"
}

variable "engine" {
  type        = string
  description = "The database engine to use."
}

variable "engine_version" {
  type        = string
  description = "The engine version to use."
}

variable "username" {
  type        = string
  description = "Username for the master DB user."
}

variable "password" {
  type        = string
  description = "Password for the master DB user."
}

variable "family" {
  type        = string
  description = "The family for the parameter group."
}

variable "major_engine_version" {
  type        = string
  description = "The major engine version for the options group."
}

variable "instance_class" {
  type        = string
  description = "The instance type of the RDS instance."
  default     = "db.t2.micro"
}

variable "port" {
  type        = string
  description = "The port on which the DB accepts connections."
  default     = ""
}

variable "allocated_storage" {
  type        = number
  description = "The allocated storage in gibibytes."
  default     = 20
}

variable "apply_immediately" {
  type        = bool
  description = "Specifies whether any database modifications are applied immediately, or during the next maintenance window."
  default     = true
}

variable "deletion_protection" {
  type        = bool
  description = "If the DB instance should have deletion protection enabled."
  default     = false
}

variable "availability_zone" {
  type        = string
  description = "The AZ for the RDS instance."
  default     = ""
}

variable "backup_retention_period" {
  type        = number
  description = "The days to retain backups for."
  default     = 0
}

variable "parameters" {
  type        = list(object({
    name = string
    value = string
  }))
  description = "A list of parameter group parameters"
  default     = []
}

variable "options" {
  type        = list(object({
    name = string
    settings = object({
      name = string
      value = string
    })
  }))
  description = "A list of options group options"
  default     = []
}

variable "tags" {
  type    = map(string)
  default = {}
}
