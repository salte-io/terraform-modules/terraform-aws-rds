locals {
  full_name = var.environment == "live" ? var.name : "${var.name}-${var.environment}"
  vpc_name  = var.public ? "salte-public" : "salte-private"

  tags = {
    application = var.name
    environment = var.environment
    terraform   = "true"
  }
}

data "aws_vpc" "default" {
  tags = {
    Name = local.vpc_name
  }
}

data "aws_security_group" "selected" {
  vpc_id = data.aws_vpc.default.id

  tags = {
    Name = local.vpc_name
  }
}

resource "aws_db_instance" "default" {
  option_group_name    = aws_db_option_group.default.name
  parameter_group_name = aws_db_parameter_group.default.name

  # Computed
  identifier             = local.full_name
  publicly_accessible    = var.public
  db_subnet_group_name   = local.vpc_name
  vpc_security_group_ids = [data.aws_security_group.selected.id]

  # Required
  name           = var.name
  engine         = var.engine
  engine_version = var.engine_version
  instance_class = var.instance_class
  port           = var.port
  username       = var.username
  password       = var.password

  # Recommended
  allocated_storage = var.allocated_storage
  apply_immediately = var.apply_immediately

  # Optional
  deletion_protection     = var.deletion_protection
  availability_zone       = var.availability_zone
  backup_retention_period = var.backup_retention_period

  storage_type                        = "gp2"                 # SSD (default)
  backup_window                       = "22:00-23:00"         # 10 PM - 11 PM
  maintenance_window                  = "Mon:00:00-Mon:03:00" # 12 AM - 3 AM - Monday Only
  auto_minor_version_upgrade          = true
  allow_major_version_upgrade         = false
  copy_tags_to_snapshot               = false
  iam_database_authentication_enabled = false
  multi_az                            = false
  skip_final_snapshot                 = true

  tags = merge(local.tags, var.tags)
}

resource "aws_db_parameter_group" "default" {
  name   = local.full_name
  family = var.family

  dynamic "parameter" {
    for_each = var.parameters

    content {
      name  = parameter.name
      value = parameter.value
    }
  }

  tags = merge(local.tags, var.tags)
}

resource "aws_db_option_group" "default" {
  name                 = local.full_name
  engine_name          = var.engine
  major_engine_version = var.major_engine_version

  dynamic "option" {
    for_each = var.options

    content {
      option_name = option.name

      option_settings {
        name  = option.settings.name
        value = option.settings.value
      }
    }
  }

  tags = merge(local.tags, var.tags)
}
