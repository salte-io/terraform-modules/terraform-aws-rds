# Terraform Module: RDS

> Simplified RDS Instances within Salte!

## Table of Contents

* [Dependencies](#dependencies)
* [Usage](#usage)
  * [Input Variables](#input-variables)
  * [Output Variables](#output-variables)
* [Author Information](#author-information)

## Dependencies

This module depends on a correctly configured [AWS Provider](https://www.terraform.io/docs/providers/aws/index.html) in your Terraform codebase.

## Usage

```tf
# ...
module "database" {
  source  = "git::https://gitlab.com/salte-io/terraform-modules/terraform-aws-rds.git?ref=1.0.1"

  name                 = "my-app"
  environment          = "live"

  engine               = "mariadb"
  engine_version       = "10.3"

  username             = "root"
  password             = "root"

  port                 = "3306"
  public               = true

  # Parameter Group Settings
  family               = "mariadb10.3"
  major_engine_version = "10.3"

  tags {
    application = "my-app"
    environment = "live"
    terraform   = "true"
  }
}
```

Then, load the module using `terraform init`.

### Input Variables

Available variables are listed below, along with their default values:

| **Variable** | **Description** | **Required?** |
| -------- | ----------- | ------- |
| `public` | Whether the DB Instance should be publicly accessible | **Yes** |
| `name` | The name of the database excluding the environment | **Yes** |
| `environment` | The current environment | **Yes** |
| `engine` | The database engine to use. | **Yes** |
| `engine_version` | The engine version to use. | **Yes** |
| `username` | Username for the master DB user. | **Yes** |
| `password` | Password for the master DB user. | **Yes** |
| `family` | The family for the parameter group. | **Yes** |
| `major_engine_version` | The major engine version for the options group. | **Yes** |
| `instance_class` | The instance type of the RDS instance. | No (defaults to `db.t2.micro`) |
| `port` | The port on which the DB accepts connections. | No (defaults to the `engines` default port value) |
| `allocated_storage` | The allocated storage in gibibytes. | No (defaults to `20`) |
| `apply_immediately` | Specifies whether any database modifications are applied immediately, or during the next maintenance window. | No (defaults to `true`) |
| `deletion_protection` | If the DB instance should have deletion protection enabled. | No (defaults to `false`) |
| `availability_zone` | The AZ for the RDS instance. | No |
| `backup_retention_period` | The days to retain backups for. | No (defaults to `0`) |
| `parameters` | A list of parameter group parameters. | No |
| `options` | A list of options group options. | No |

### Output Variables

Available output variables are listed below:

| **Variable** | **Description** |
| -------- | ----------- |
| `id` | The ID of the DB Instance. |
| `address` | The Address of the DB Instance. |
| `endpoint` | The DNS Endpoint of the DB Instance. |

## Author Information

This module is currently maintained by the individuals listed below.

* [Nick Woodward](https://github.com/nick-woodward)
