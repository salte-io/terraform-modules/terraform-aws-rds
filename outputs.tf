output "id" {
  value = aws_db_instance.default.id
  description = "ID of the instance"
}

output "address" {
  value = aws_db_instance.default.address
  description = "Address of the instance"
}

output "endpoint" {
  value = aws_db_instance.default.endpoint
  description = "DNS Endpoint of the instance"
}
